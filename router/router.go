package router

import (
	"blockparty/database"
	_ "blockparty/docs"
	"blockparty/utils"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/redirect"
	"github.com/gofiber/swagger"
	log "github.com/sirupsen/logrus"
	"sync"
)

type CIDsList struct {
	Cids []string `json:"cids"`
}

func SetupApp() *fiber.App {
	app := fiber.New()

	app.Use(redirect.New(redirect.Config{
		Rules: map[string]string{
			"/": "/swagger",
		},
		StatusCode: 301,
	}))

	app.Get("/swagger/*", swagger.HandlerDefault)

	app.Get("/health", func(c *fiber.Ctx) error {
		return c.Status(fiber.StatusOK).Send(nil)
	})

	app.Post("/readcsvfile", executeReadCSVFile)
	app.Post("/append", appendEndpoint)
	app.Get("/tokens/:cid", getByCIDEndpoint)
	app.Get("/tokens", getAllEndpoint)

	return app
}

// @Summary Execute the function ScrapeCVSFile
// @Description Read and process the ipfs_cids.csv file
// @Tags CSV
// @Produce  json
// @Success 200 {object} map[string]interface{}
// @Failure 422 {object} map[string]interface{}
// @Router /readcsvfile [post]
func executeReadCSVFile(c *fiber.Ctx) error {
	invalidsURL := fiber.Map{}
	for _, aerr := range utils.ScrapeCVSFile() {
		invalidsURL[aerr.Cid] = aerr.Error.Error()
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"errors": invalidsURL,
	})
}

// @Summary Append CIDs
// @Description Append a list of CIDs to be processed
// @Tags CIDs
// @Accept  json
// @Produce  json
// @Param cids body CIDsList true "List of CIDs"
// @Success 200 {object} map[string]interface{}
// @Failure 422 {object} map[string]interface{}
// @Router /append [post]
func appendEndpoint(c *fiber.Ctx) error {
	var cidList CIDsList
	var err error

	if err = c.BodyParser(&cidList); err != nil {
		log.Error(err)
		return c.Status(fiber.StatusUnprocessableEntity).JSON(fiber.Map{
			"errors": err.Error(),
		})
	}
	invalidsURL := fiber.Map{}
	var wg sync.WaitGroup
	errCh := make(chan utils.CIDError)

	for _, cid := range cidList.Cids {
		wg.Add(1)
		go utils.ProcessCID(cid, &wg, errCh)
	}
	go func() {
		wg.Wait()
		close(errCh)
	}()

	for aerr := range errCh {
		invalidsURL[aerr.Cid] = aerr.Error.Error()
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"errors": invalidsURL,
	})
}

// @Summary Get CID
// @Description Get metadata by CID store in DynamoDB
// @Tags CIDs
// @Produce  json
// @Param cid path string true "CID"
// @Success 200 {object} database.Metadata
// @Failure 422 {object} map[string]interface{}
// @Router /tokens/{cid} [get]
func getByCIDEndpoint(c *fiber.Ctx) error {
	cid := c.Params("cid")
	err, metadata := database.GetOneCommand(cid)
	if err != nil {
		return c.Status(fiber.StatusUnprocessableEntity).JSON(fiber.Map{
			"errors": err.Error(),
		})
	}
	return c.JSON(metadata)
}

// @Summary Get all CIDs
// @Description Get metadata for all CIDs store in DynamoDB
// @Tags CIDs
// @Produce  json
// @Success 200 {array} []database.Metadata
// @Failure 422 {object} map[string]interface{}
// @Router /tokens [get]
func getAllEndpoint(c *fiber.Ctx) error {
	err, metadatas := database.GetAllCommand()
	if err != nil {
		return c.Status(fiber.StatusUnprocessableEntity).JSON(fiber.Map{
			"errors": err.Error(),
		})
	}
	return c.JSON(metadatas)
}
