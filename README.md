# BlockParty Test

For this project, I chose to use Golang, paired with DynamoDB for data storage. The application is containerized using Docker and deployed via the Amazon ECS service. For logging purposes, CloudWatch has been integrated. I believe this combination of technologies aligns well with the project's requirements.

<details>
<summary>Project Definition</summary>

## Project Overview

The objective is to design and implement a microservice using either Golang or Python. This service should scrape metadata from a list of provided IPFS URIs and store this data in an AWS database service such as DynamoDB, RDS, or Postgres running on EC2. Furthermore, this service should provide two RESTful API endpoints to retrieve the stored data.

## Key Tasks

1. Create a metadata scraper in either Golang or Python to retrieve information from the supplied list of IPFS CIDs.
2. Integrate an AWS SDK within your service (Golang/Python) to connect to a DynamoDB (or RDS) instance and store the retrieved data.
3. Design a Golang-based RESTful API that offers two endpoints:
   - `GET /tokens`: Fetches all data from the database and returns it in JSON format.
   - `GET /tokens/:id`: Fetches data for a specific IPFS CID.
4. Containerize the service using Docker.

</details>

## AWS Services Solution Architecture

```mermaid
graph TD;
B[AWS];
C[ecs_cluster.cluster];
D[ecs_service.service] --> C;
D --> E[ecs_task_definition.task];
D --> F[security_group.ecs_sg];
D --> G[subnet.public_subnet];
H[internet_gateway.main] --> I[vpc.main];
J[route_table.public_route_table] --> H;
K[route_table_association.public_subnet_association] --> J;
K --> G;
F --> I;
G --> I;
B --> D;
B --> K;
C --> A[dynamodb_table.table];
C --> L[cloudwatch];
```

This architecture eliminates the need to share secrets when accessing resources like DynamoDB and CloudWatch. However, for non-Amazon resources, it's crucial to employ AWS Secrets Manager and feed them to the container as environment variables. Assigning a public IP to each container isn't recommended; a more secure approach would be to use a load balancer along with other access restriction techniques. For the purposes of this project, however, the chosen architecture suffices.

## Deployment Process

Terraform was selected for deployment due to its efficiency in creating and dismantling the project's infrastructure. It needs to be installed beforehand. [Learn more about installing Terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli).

1. Define the following Amazon-related environment variables:
   * `AWS_ACCESS_KEY_ID`
   * `AWS_SECRET_ACCESS_KEY`
2. Execute these commands:

   ```bash
   terraform init    # Downloads dependencies
   terraform plan    # Generates an execution plan
   terraform apply   # Enforces the changes outlined in main.tf
   ```
3. After deployment, choose a method to retrieve the public IP for accessing the app:

    <details>
    <summary>Via AWS CLI</summary>

   1. Configure AWS-CLI for the us-east-2 region.
   2. Execute the following command.
       ```bash
       PUBLIC_IP=$(aws ec2 describe-network-interfaces --network-interface-ids $(aws ecs describe-tasks --cluster cluster --tasks $(aws ecs list-tasks --cluster cluster --service-name service --query 'taskArns[0]' --output text) --query 'tasks[0].attachments[0].details[?name==`networkInterfaceId`].value' --output text) --query 'NetworkInterfaces[0].Association.PublicIp' --output text) && echo $PUBLIC_IP
       ```
   3. Use the output IP in your browser or Postman.
    </details>

    <details>
    <summary>Through AWS Web Console</summary>

   1. Log into the AWS Management Console.
   2. Go to the Amazon ECS Service.
   3. Choose the `cluster` Cluster.
   4. Navigate to the `Tasks` section.
   5. Select the active task ID.
   6. Locate the Public IP within the configuration.
   7. Open using the provided link or copy the IP for use in your browser or Postman.
    </details>

4. Begin exploring and testing the application.
5. When finished, destroy all AWS resources with:
    ```bash
    terraform destroy
    ```
## Hey there 🤓

I hope diving into this README was as fun as a successful debug session.

For any further discussions or queries, feel free to drop me an email at [jf@coterey.com](mailto:jf@coterey.com).

Happy coding! 👨‍💻👩‍💻
