package main

import (
	"blockparty/database"
	"blockparty/router"
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"
	"github.com/stretchr/testify/assert"
	"io"
	"log"
	"net/http/httptest"
	"os"
	"testing"
)

var app *fiber.App

func TestMain(m *testing.M) {

	err := os.Setenv("AWS_ACCESS_KEY_ID", "DUMMYEXAMPLEID")
	if err != nil {
		return
	}
	err = os.Setenv("AWS_SECRET_ACCESS_KEY", "DUMMYEXAMPLEKEY")
	if err != nil {
		log.Fatalf("Could not connect to Docker: %s", err)
	}
	err = os.Setenv("DYNAMODB_LOCAL_URL", "http://localhost:58000")
	if err != nil {
		log.Fatalf("Could not connect to Docker: %s", err)
	}

	app = router.SetupApp()

	// uses a sensible default on windows (tcp/http) and linux/osx (socket)
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not construct pool: %s", err)
	}

	// uses pool to try to connect to Docker
	err = pool.Client.Ping()
	if err != nil {
		log.Fatalf("Could not connect to Docker: %s", err)
	}

	resource, err := pool.RunWithOptions(&dockertest.RunOptions{
		Name:       "dynamoDBTest",
		Repository: "amazon/dynamodb-local",
		Tag:        "2.0.0",
		PortBindings: map[docker.Port][]docker.PortBinding{
			"8000/tcp": {{HostIP: "", HostPort: "58000"}},
		},
	})
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}

	if err := pool.Retry(func() error {
		err = database.Init()
		if err != nil {
			return err
		}
		return nil
	}); err != nil {
		if err := pool.Purge(resource); err != nil {
			log.Fatalf("Could not purge resource: %s", err)
		}
		log.Fatalf("Could not connect to database: %s", err)
	}

	code := m.Run()

	if err := pool.Purge(resource); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}

	os.Exit(code)
}

func TestAppendEndPoint(t *testing.T) {
	requestBody, err := json.Marshal(map[string][]string{
		"cids": {
			"bafkreifovhtvvrx5jmo2b4ne2hoyk4t3c276jc7weva5s57ilupiiuqg2y",
			"bafkreifwbyviygstiqmjcijju33r6scctuyxciqiepcrs2ym2bbpf7c7rq",
		},
	})

	req := httptest.NewRequest("POST", "/append", bytes.NewBuffer(requestBody))
	req.Header.Set("Content-Type", "application/json")
	resp, err := app.Test(req, -1)
	if err != nil {
		t.Fatalf("Failed to make test request: %s", err.Error())
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("Failed to read response body: %s", err.Error())
	}

	var responseBody = struct {
		Errors map[string]string `json:"errors"`
	}{}
	err = json.Unmarshal(body, &responseBody)
	if err != nil {
		t.Fatalf("Failed to unmarshal response body: %s", err.Error())
	}

	assert.Equal(t, fiber.StatusOK, resp.StatusCode)
	assert.Len(t, responseBody.Errors, 0)
}

func TestGetByCIDEndPoint(t *testing.T) {
	cid := "bafkreifovhtvvrx5jmo2b4ne2hoyk4t3c276jc7weva5s57ilupiiuqg2y"
	req := httptest.NewRequest("GET", "/tokens/"+cid, nil)
	resp, err := app.Test(req, -1)
	if err != nil {
		t.Fatalf("Failed to make test request: %s", err.Error())
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("Failed to read response body: %s", err.Error())
	}
	var responseBody database.Metadata
	err = json.Unmarshal(body, &responseBody)
	if err != nil {
		t.Fatalf("Failed to unmarshal response body: %s", err.Error())
	}

	assert.Equal(t, fiber.StatusOK, resp.StatusCode)
	assert.NotEmpty(t, responseBody)
	assert.Equal(t, responseBody.CID, cid)
}

func TestGetAllEndPoint(t *testing.T) {
	req := httptest.NewRequest("GET", "/tokens", nil)
	resp, err := app.Test(req, -1)
	if err != nil {
		t.Fatalf("Failed to make test request: %s", err.Error())
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("Failed to read response body: %s", err.Error())
	}
	var responseBody []database.Metadata
	err = json.Unmarshal(body, &responseBody)
	if err != nil {
		t.Fatalf("Failed to unmarshal response body: %s", err.Error())
	}

	assert.Equal(t, fiber.StatusOK, resp.StatusCode)
	assert.Len(t, responseBody, 2)

}
