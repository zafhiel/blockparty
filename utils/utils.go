package utils

import (
	"blockparty/database"
	"encoding/csv"
	"encoding/json"
	"errors"
	log "github.com/sirupsen/logrus"
	"io"
	"net/http"
	"os"
	"sync"
)

var ErrInvalidCID = errors.New("CID is invalid or does not exist")

type CIDError struct {
	Cid   string
	Error error
}

func scrape(cid string) (error, database.Metadata) {
	url := "https://blockpartyplatform.mypinata.cloud/ipfs/" + cid

	resp, err := http.Get(url)
	if err != nil {
		log.Error(err)
		return err, database.Metadata{}
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			log.Error(err)
		}
	}()

	if resp.StatusCode != http.StatusOK {
		return ErrInvalidCID, database.Metadata{}
	}

	var metadata database.Metadata
	err = json.NewDecoder(resp.Body).Decode(&metadata)
	if err != nil {
		log.Error(err)
		return err, database.Metadata{}
	}

	metadata.CID = cid

	return nil, metadata
}

func ScrapeCVSFile() []CIDError {
	log.Info("Reading ipfs_cids.csv file")

	csvFile, err := os.Open("ipfs_cids.csv")
	if err != nil {
		log.WithField("error", err).Error("Unable to open CSV file")
		return []CIDError{}
	}
	r := csv.NewReader(csvFile)

	var wg sync.WaitGroup
	errCh := make(chan CIDError)

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			errCh <- CIDError{"cvf file", err}
			continue
		}
		wg.Add(1)
		go ProcessCID(record[0], &wg, errCh)
	}

	go func() {
		wg.Wait()
		close(errCh)
	}()

	var result []CIDError
	for aerr := range errCh {
		result = append(result, aerr)
	}
	return result
}

func ProcessCID(cid string, wg *sync.WaitGroup, errCh chan CIDError) {
	defer wg.Done()
	err, metadata := scrape(cid)
	if err != nil {
		errCh <- CIDError{cid, err}
		return
	} else {
		err = database.PutCommand(metadata)
		if err != nil {
			errCh <- CIDError{cid, err}
			return
		}
	}
}
