########################################################################################################################
## Build BlockParty Project
########################################################################################################################
FROM golang:1.20-alpine3.18 as blockpartyBuilder

WORKDIR /go/src/blockparty/

COPY go.mod ./
COPY go.sum ./

RUN go mod download

COPY ./ ./

RUN go build -o blockparty .

########################################################################################################################
## Copy API Build to Deployment Container
########################################################################################################################
FROM alpine:3.18
RUN mkdir -p /app && \
    apk add --no-cache tzdata curl
WORKDIR /app

ENV TZ=America/New_York

COPY ipfs_cids.csv .
COPY --from=blockpartyBuilder /go/src/blockparty/blockparty .

EXPOSE 80

ENTRYPOINT ["./blockparty"]
