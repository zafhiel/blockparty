package main

import (
	"blockparty/router"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
	logrusCloudwatchLogs "github.com/kdar/logrus-cloudwatchlogs"
	log "github.com/sirupsen/logrus"
	"os"
)

// @title BlockParty Test
// @version 1.0
// @description This is the Swagger documentation for the BlockParty API. For additional information, please visit https://gitlab.com/zafhiel/blockparty
// @termsOfService http://swagger.io/terms/
// @contact.name Jhon Fredy Cote
// @contact.email jf@coterey.com
// @BasePath /
func main() {
	err, hook, formater := handleCloudWatchLogs()

	if err == nil {
		log.AddHook(hook)
		log.SetFormatter(formater)
	} else {
		log.WithField("error", err).Warn("Error Connecting CloudWatch")
	}

	log.Info("Starting app")

	app := router.SetupApp()

	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = "80"
	}
	err = app.Listen(":" + port)
	if err != nil {
		log.WithField("error", err).Error("Failed to start server")
	}
}

func handleCloudWatchLogs() (error, *logrusCloudwatchLogs.Hook, *logrusCloudwatchLogs.ProdFormatter) {
	group := "app"
	stream := "single-instance"

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-east-2"),
	})

	if err != nil {
		return err, nil, nil
	}

	_, err = sts.New(sess).GetCallerIdentity(&sts.GetCallerIdentityInput{})
	if err != nil {
		return err, nil, nil
	}

	hook, err := logrusCloudwatchLogs.NewHook(group, stream, sess)
	if err != nil {
		return err, nil, nil
	}

	return nil, hook, logrusCloudwatchLogs.NewProdFormatter()
}
