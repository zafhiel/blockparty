package database

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	log "github.com/sirupsen/logrus"
	"os"
)

type Metadata struct {
	CID         string `json:"cid"`
	Image       string `json:"image"`
	Description string `json:"description"`
	Name        string `json:"name"`
}

var svc *dynamodb.DynamoDB

func getDynamoDBSession() *dynamodb.DynamoDB {
	if svc == nil {
		if err := Init(); err != nil {
			log.WithField("error", err).Error("Failed to create AWS session")
		}
	}
	return svc
}

func Init() error {
	dynamoLocalUrl := os.Getenv("DYNAMODB_LOCAL_URL")

	sessionConfig := &aws.Config{
		Region: aws.String("us-east-2"),
	}

	if len(dynamoLocalUrl) != 0 {
		sessionConfig.Endpoint = aws.String(dynamoLocalUrl)
	}

	sess, err := session.NewSession(sessionConfig)

	if err != nil {
		log.WithField("error", err).Error("Failed to create AWS session")
		return err
	}

	svc = dynamodb.New(sess)

	if err = CheckTable(); err != nil {
		return err
	}
	return nil
}

func PutCommand(metadata Metadata) error {
	err, exist := GetOneCommand(metadata.CID)
	if err != nil || exist.CID != "" {
		return nil
	}

	log.WithField("metadata", metadata).Info("Store into DynamoDB")
	av, err := dynamodbattribute.MarshalMap(metadata)
	if err != nil {
		log.WithField("error", err).Error("Failed to marshal metadata")
		return err
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String("MetadataTable"),
	}

	_, err = getDynamoDBSession().PutItem(input)
	if err != nil {
		log.WithField("error", err).Error("Failed to put item")
		return err
	}

	return nil
}

func GetOneCommand(cid string) (error, Metadata) {
	log.WithField("cid", cid).Info("Get Metadata by CID")
	input := &dynamodb.GetItemInput{
		TableName: aws.String("MetadataTable"),
		Key: map[string]*dynamodb.AttributeValue{
			"cid": {
				S: aws.String(cid),
			},
		},
	}

	result, err := getDynamoDBSession().GetItem(input)
	if err != nil {
		log.Error(err)
		return err, Metadata{}
	}

	metadata := Metadata{}
	err = dynamodbattribute.UnmarshalMap(result.Item, &metadata)
	if err != nil {
		log.Error(err)
		return err, Metadata{}
	}

	return nil, metadata
}

func GetAllCommand() (error, []Metadata) {
	log.Info("Get all CIDs Metadata")
	input := &dynamodb.ScanInput{
		TableName: aws.String("MetadataTable"),
	}

	result, err := getDynamoDBSession().Scan(input)
	if err != nil {
		log.Error(err)
		return err, []Metadata{}
	}

	var metadatas []Metadata
	for _, item := range result.Items {
		metadata := Metadata{}
		err = dynamodbattribute.UnmarshalMap(item, &metadata)
		if err != nil {
			log.Error(err)
			return err, []Metadata{}
		}
		metadatas = append(metadatas, metadata)
	}

	return nil, metadatas
}

func CheckTable() error {
	// Create table if it doesn't exist
	_, err := getDynamoDBSession().DescribeTable(&dynamodb.DescribeTableInput{
		TableName: aws.String("MetadataTable"),
	})

	if err != nil {
		log.WithField("error", err).Warn("Creating MetadataTable in DynamoDB")
		input := &dynamodb.CreateTableInput{
			AttributeDefinitions: []*dynamodb.AttributeDefinition{
				{
					AttributeName: aws.String("cid"),
					AttributeType: aws.String("S"),
				},
			},
			KeySchema: []*dynamodb.KeySchemaElement{
				{
					AttributeName: aws.String("cid"),
					KeyType:       aws.String("HASH"),
				},
			},
			ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
				ReadCapacityUnits:  aws.Int64(5),
				WriteCapacityUnits: aws.Int64(5),
			},
			TableName: aws.String("MetadataTable"),
		}

		_, err = getDynamoDBSession().CreateTable(input)
		if err != nil {
			return err

		}
	}
	return nil
}
